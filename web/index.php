<?php
// enable output of errors
ini_set('display_errors', true);
error_reporting(E_ALL | E_STRICT);

// defines
define('YII_DEBUG', true);
define('APP_PATH_ROOT', realpath(__DIR__ . '/..'));

// main functions file
require APP_PATH_ROOT . '/lib/functions.php';
require APP_PATH_ROOT . '/vendor/autoload.php';
require APP_PATH_ROOT . '/vendor/yiisoft/yii2/Yii.php';

$application = new \yii\web\Application(require APP_PATH_ROOT . '/config/web.php');
$application->run();