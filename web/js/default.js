'use strict';

/**
 * This uses the following formula: weight x reps x .0333 + weight = 1RM.
 * Courtesy of Jim Wendler and his 5/3/1 strength program.
 *
 * @param {Number} weight
 * @param {Number} reps
 * @returns {*}
 */
function calcOrm(weight, reps) {
    var orm;

    // parse weight
    weight = parseFloat(weight);
    if (isNaN(weight)) {
        return 0;
    }

    // parse reps
    reps = parseInt(reps);
    if (isNaN(reps)) {
        return 0;
    }

    // calc orm
    orm = weight * (1 + reps * 0.0333);
    // orm = weight * reps * 0.0333 + weight;

    console.log({w: weight, r: reps, orm: orm});

    return Math.round(orm);
}

var $calcOrm = $('#orm-calc');
$calcOrm.find('input[type=text]').on('keyup', function () {
    var weight = $calcOrm.find('input[name=weight]').val(),
        reps = $calcOrm.find('input[name=reps]').val(),
        orm = calcOrm(weight, reps);

    $calcOrm.find('#orm-result > div').html(orm);
});