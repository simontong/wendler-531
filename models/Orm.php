<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orm".
 *
 * @property string $orm_id
 * @property string $exercise_id
 * @property double $weight
 *
 * @property Exercise $exercise
 */
class Orm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exercise_id'], 'integer'],
            [['weight'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'orm_id' => 'Orm ID',
            'exercise_id' => 'Exercise ID',
            'weight' => 'Weight',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExercise()
    {
        return $this->hasOne(Exercise::className(), ['exercise_id' => 'exercise_id']);
    }
}
