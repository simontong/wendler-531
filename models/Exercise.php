<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "exercise".
 *
 * @property string $exercise_id
 * @property string $name
 * @property double $increment
 *
 * @property Orm $orm
 */
class Exercise extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exercise';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'integer'],
            [['increment'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'exercise_id' => 'Exercise ID',
            'name' => 'Name',
            'increment' => 'Increment',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrm()
    {
        return $this->hasOne(Orm::className(), ['exercise_id' => 'exercise_id']);
    }

    /**
     * @param int $cycle
     * @return array
     */
    public function getWeights($cycle)
    {
        $ormPercent = 90;

        $orm = $this->orm->weight + ($this->increment * ($cycle - 1));
        $orm = ($ormPercent / 100) * $orm;

        // matrix for cycle
        $weeks = [
            1 => [
                'reps' => [
                    [5, 65],
                    [5, 75],
                    [5, 85],
                ],
                'lastSetMax' => true,
            ],
            2 => [
                'reps' => [
                    [3, 70],
                    [3, 80],
                    [3, 90],
                ],
                'lastSetMax' => true,
            ],
            3 => [
                'reps' => [
                    [5, 75],
                    [3, 85],
                    [1, 95],
                ],
                'lastSetMax' => true,
            ],
            4 => [
                'reps' => [
                    [5, 40],
                    [5, 50],
                    [5, 60],
                ],
                'lastSetMax' => false,
            ],
        ];

        // calculate weights for this week
        $weights = [];
        foreach ($weeks as $weekNo=>$week)
        {
            foreach ($week['reps'] as $reps)
            {
                $percent = $reps[1];

                $weight = ($percent / 100) * $orm;
                $weights[$weekNo]['531'][] = round($weight / $this->increment) * $this->increment;
            }

            // add boring but big
//            $weights[$weekNo]['bbb'] = $orm * 0.5 + (10 * ($cycle - 1));
        }

        return $weights;
    }
}
