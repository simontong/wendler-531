<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class AppAsset
 * @package app
 *
 * @author Simon Tong <simon@supplyware.co.uk>
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/default.css',
    ];
    public $js = [
        'js/default.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}