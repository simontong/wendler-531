<?php
return array_merge_recursive(require APP_PATH_ROOT . '/config/main.php', [
    'controllerNamespace' => 'app\commands',
]);