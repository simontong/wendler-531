<?php
return [
    'id' => 'wendler-531',
    'basePath' => APP_PATH_ROOT,
    'bootstrap' => ['log', 'gii'],
    'extensions' => require APP_PATH_ROOT . '/vendor/yiisoft/extensions.php',
    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['192.168.56.1', '127.0.0.1', '::1'],
        ],
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'db' => [
            'class' => 'yii\db\Connection',
            'charset' => 'utf8',
            'dsn' => 'mysql:host=localhost;dbname=supplyware_wendler531',
            'username' => 'root',
            'password' => 'root',
            'on afterOpen' => function ($event)
            {
//                $event->sender->createCommand("SET time_zone = '" . date('P') . "'")->execute();
                $event->sender->createCommand("set time_zone='+00:00';")->execute();
            },
            'enableSchemaCache' => true,
            'schemaCacheDuration' => YII_DEBUG ? 0 : 86400000, // 1000 days
        ],
    ],
];