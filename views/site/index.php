<?php
/**
 * @var $this \yii\web\View
 * @var $exercises \app\models\Exercise[]
 * @var $orms \app\models\Orm[]
 * @var $cycle int
 */
use yii\helpers\Html;

?>
<div class="row">
    <div class="col-md-2">
        <h2>Cycles</h2>
        <ul>
            <?php
            for ($x = 1; $x <= 12; $x++)
            {
                echo '<li' . ($cycle == $x ? ' class="active"' : '') . '>'
                    . Html::a("Cycle $x", ['/site/index', 'cycle' => $x])
                    . '</li>';
            }
            ?>
        </ul>

        <h3>ORMs</h3>
        <ul>
            <?php
            foreach ($orms as $orm)
            {
                echo '<li>' . $orm->exercise->name . ': <span class="badge pull-right">' . $orm->weight . ' kg</span></li>';
            }
            ?>
        </ul>

        <h3>ORM Calc</h3>
        <form id="orm-calc" class="form-horizontal" role="form">
            <div class="form-group form-group-sm">
                <label class="control-label col-sm-4">Weight:</label>
                <div class="col-sm-8">
                    <input type="text" name="weight" class="form-control" />
                </div>
            </div>
            <div class="form-group form-group-sm">
                <label class="control-label col-sm-4">Reps:</label>
                <div class="col-sm-8">
                    <input type="text" name="reps" class="form-control" />
                </div>
            </div>
            <div class="form-group form-group-sm" id="orm-result">
                <label class="control-label col-sm-4">ORM:</label>
                <div class="col-sm-8 form-control-static">0</div>
            </div>
        </form>

    </div>
    <div class="col-md-10">
        <h2><?= "Cycle $cycle"; ?></h2>
        <table class="table table-hover table-responsive">
            <thead>
            <tr>
                <th>Exercise</th>
                <th>Week</th>
                <th>5/3/1 Weight</th>
                <?php /*<th>BBB Weight (5x10)</th>*/ ?>
            </tr>
            </thead>
            <tbody>
            <?php
            $lastExerciseId = null;
            foreach ($exercises as $i => $exercise)
            {
            foreach ($exercise->getWeights($cycle) as $weekNo => $weeks)
            {
            ?>
            <tr<?php if ($i % 2 == 0)
            {
                echo ' class="warning"';
            } ?>>
                <td>
                    <?php
                    if ($lastExerciseId != $exercise->exercise_id)
                    {
                        $lastExerciseId = $exercise->exercise_id;
                        echo $exercise->name;
                    }
                    ?>
                </td>
                <td><?= $weekNo; ?></td>
                <td>
                    <?php
                    foreach ($weeks['531'] as $weight)
                    {
                        echo '<span class="label label-info">' . $weight . '</span>&nbsp; ';
                    }
                    ?>
                </td>
                <?php /*<td><span class="label label-info"><?= $weeks['bbb']; ?></span></td>*/ ?>
            </tr>
            </tbody>
            <?php
            }
            }
            ?>
        </table>

    </div>
</div>