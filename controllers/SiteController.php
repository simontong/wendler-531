<?php
namespace app\controllers;

use app\models\Exercise;
use app\models\Orm;
use yii\web\Controller;

/**
 * Class SiteController
 * @package app\controllers
 *
 * @author Simon Tong <simon@supplyware.co.uk>
 */
class SiteController extends Controller
{
    /**
     * @param int $cycle
     * @return string
     */
    public function actionIndex($cycle = 1)
    {
        $exercises = Exercise::find()->all();
        $orms = Orm::find()->all();

        return $this->render('index', compact('cycle', 'orms', 'exercises'));
    }
}