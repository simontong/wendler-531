<?php

use yii\db\Schema;

class m151228_082643_create_table_exercise extends \yii\db\Migration
{
    public function up()
    {
        return $this->createTable('exercise', [
            'exercise_id' => 'int(10) unsigned auto_increment',
            'name' => 'varchar(255)',
            'increment' => 'float(5,2) unsigned',
            "PRIMARY KEY (exercise_id)",
        ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8'
        );
    }

    public function down()
    {
        return $this->dropTable('exercise');
    }
}
