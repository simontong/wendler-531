<?php

use yii\db\Schema;

class m151228_083313_create_table_orm extends \yii\db\Migration
{
    public function up()
    {
        return $this->createTable('orm', [
            'orm_id' => 'int(10) unsigned auto_increment',
            'exercise_id' => 'int(10) unsigned',
            'weight' => 'float(5,2) unsigned',
            "PRIMARY KEY (orm_id)",
            'KEY exercise_id (exercise_id)',
            'CONSTRAINT fk_orm_exercise_exercise_id FOREIGN KEY (exercise_id) REFERENCES exercise (exercise_id)',
        ],
            'ENGINE=InnoDB DEFAULT CHARSET=utf8'
        );
    }

    public function down()
    {
        return $this->dropTable('orm');
    }
}
