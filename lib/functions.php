<?php
/**
 * @param $data
 * @param bool $terminate
 * @param bool $htmlChars
 * @param bool $return
 * @param string $wrapTag
 * @return mixed|null|string
 */
function pr($data, $terminate = true, $htmlChars = false, $return = false, $wrapTag = 'pre')
{
    // set output (could be array, object etc.)
    $data = print_r($data, 1);
//    $data = var_export($data,1);

    // add html specialchars
    if ($htmlChars)
    {
        $data = htmlspecialchars($data);
    }

    // return the result or output it
    if ($return)
    {
        return $data;
    }
    else
    {
        if ($wrapTag && !((substr(PHP_SAPI, 0, 3) == 'cgi' || PHP_SAPI == 'cli') && empty($_SERVER['REMOTE_ADDR'])))
        {
            echo "<$wrapTag>\n$data\n</$wrapTag>";
        }
        else
        {
            echo $data . PHP_EOL;
        }
    }

    // terminate app
    if ($terminate)
    {
        exit(1);
    }

    return null;
}